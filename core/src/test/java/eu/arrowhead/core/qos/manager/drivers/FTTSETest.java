/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.arrowhead.core.qos.manager.drivers;

import java.util.HashMap;
import java.util.Map;
import junit.framework.TestCase;

/**
 *
 * @author Paulo
 */
public class FTTSETest extends TestCase {

	public FTTSETest(String testName) {
		super(testName);
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
	}

	@Override
	protected void tearDown() throws Exception {
		super.tearDown();
	}

	/**
	 * HARD REAL TIME
	 */
	public void testGenerateCommands_hardRealTime() {
		System.out.println("generateCommands");
		Integer streamID = 1;
		Integer elementaryCycle = 20;
		Map<String, String> requestedQoS = new HashMap<>();
		requestedQoS.put("delay", "100");
		requestedQoS.put("bandwidth", "100");
		requestedQoS.put("hardRealTime", "true");

		FTTSE instance = new FTTSE();
		Map<String, String> expResult = new HashMap<>();
		expResult.put("ID", "1");
		expResult.put("SIZE", "10");
		expResult.put("PERIOD", "5");
		expResult.put("SYNCHRONOUS", "1");

		Map<String, String> result = instance.generateCommands(streamID, elementaryCycle, 1450, requestedQoS);

		if (result == null) {
			System.out.println("\n Not Possible\n");
		}

		assertEquals(expResult, result);

	}

	/**
	 * SOFT REAL TIME
	 */
	public void testGenerateCommands_softRealTime() {
		System.out.println("generateCommands");
		Integer streamID = 1;
		Integer elementaryCycle = 10;
		Map<String, String> requestedQoS = new HashMap<>();

		requestedQoS.put("delay", "30");
		requestedQoS.put("bandwidth", "100");
		requestedQoS.put("hardRealTime", "false");

		FTTSE instance = new FTTSE();
		Map<String, String> expResult = new HashMap<>();
		expResult.put("ID", "1");
		expResult.put("SIZE", "3");
		expResult.put("PERIOD", "3");
		expResult.put("SYNCHRONOUS", "2");

		Map<String, String> result = instance.generateCommands(streamID, elementaryCycle, 1450, requestedQoS);

		if (result == null) {
			System.out.println("\n Not Possible\n");
		}

		assertEquals(expResult, result);

	}

	/**
	 * DEFAULT
	 */
	public void testGenerateCommands_NoSynchronousType() {
		System.out.println("generateCommands");
		Integer streamID = 1;
		Integer elementaryCycle = 20;
		Map<String, String> requestedQoS = new HashMap<>();

		requestedQoS.put("delay", "100");
		requestedQoS.put("bandwidth", "100");

		FTTSE instance = new FTTSE();
		Map<String, String> expResult = new HashMap<>();
		expResult.put("ID", "1");
		expResult.put("SIZE", "10");
		expResult.put("PERIOD", "5");
		expResult.put("SYNCHRONOUS", "1");

		Map<String, String> result = instance.generateCommands(streamID, elementaryCycle, 1450, requestedQoS);

		if (result == null) {
			System.out.println("\n Not Possible\n");
		}

		assertEquals(expResult, result);

	}

	/**
	 * SOFT REAL TIME
	 */
	public void testGenerateCommands_() {
		System.out.println("generateCommands");
		Integer streamID = 1;
		Integer elementaryCycle = 20;
		Map<String, String> requestedQoS = new HashMap<>();

		requestedQoS.put("delay", "100");
		requestedQoS.put("bandwidth", "100");

		FTTSE instance = new FTTSE();
		Map<String, String> expResult = new HashMap<>();
		expResult.put("ID", "1");
		expResult.put("SIZE", "10");
		expResult.put("PERIOD", "5");
		expResult.put("SYNCHRONOUS", "1");

		Map<String, String> result = instance.generateCommands(streamID, elementaryCycle, 1450, requestedQoS);

		if (result == null) {
			System.out.println("\n Not Possible\n");
		}

		assertEquals(expResult, result);

	}

}
