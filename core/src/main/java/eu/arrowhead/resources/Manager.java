package eu.arrowhead.resources;

import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import eu.arrowhead.common.exception.DriverNotFoundException;
import eu.arrowhead.common.exception.ReservationException;
import eu.arrowhead.common.model.messages.FilterQoSReservations;
import eu.arrowhead.common.model.messages.FilterQoSReservationsResponse;
import eu.arrowhead.common.model.messages.QoSReservationResponse;
import eu.arrowhead.common.model.messages.QoSReserve;
import eu.arrowhead.common.model.messages.QoSVerificationResponse;
import eu.arrowhead.common.model.messages.QoSVerify;

@Path("qosmanager")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class Manager {

	private static Logger log = Logger.getLogger(Manager.class.getName());
	private final QoSManagerService service = new QoSManagerService();
	
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public String home() {
		return "I am online!";
	}

	@PUT
	@Path("/QoSVerify")
	public Response qosVerification(QoSVerify qosVerify) {		
		QoSVerificationResponse qvr = service.qoSVerify(qosVerify);
		return Response.status(Status.OK).entity(qvr).build();
	}

	@PUT
	@Path("/QoSReserve")
	public Response qosReservation(QoSReserve qosReservation) throws ReservationException, DriverNotFoundException {
		log.info("QoS: Reserving resouces.");
		QoSReservationResponse qosrr = service.qoSReserve(qosReservation);
		return Response.status(Status.OK).entity(qosrr).build();
	}
	
	@DELETE
	@Path("/QoSReserve")
	public Response deletionQosReservation(QoSReserve qosReservation) throws ReservationException, DriverNotFoundException {
		log.info("QoS: Reserving resouces.");
		Boolean res = service.deleteQoSReserve(qosReservation);
		String r = res ? "QoSReservation Sucessfully Deleted" : "QoSReservation Could Not Be Deleted";
		return Response.status(Status.OK).entity(r).build();
	}
	
	@PUT
	@Path("/filterReservations")
	public Response filterQosReservation(FilterQoSReservations filter) {
		log.info("QoS: Filtering resouces.");
		FilterQoSReservationsResponse fqrr = service.filterReservation(filter);
		return Response.status(Status.OK).entity(fqrr).build();
	}
	
}
