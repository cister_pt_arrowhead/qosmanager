package eu.arrowhead.resources;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import org.hibernate.cfg.NotYetImplementedException;

import eu.arrowhead.common.exception.DriverNotFoundException;
import eu.arrowhead.common.exception.ReservationException;
import eu.arrowhead.common.model.ArrowheadSystem;
import eu.arrowhead.common.model.messages.FilterQoSReservations;
import eu.arrowhead.common.model.messages.FilterQoSReservationsResponse;
import eu.arrowhead.common.model.messages.QoSMonitorAddRule;
import eu.arrowhead.common.model.messages.QoSReservationCommand;
import eu.arrowhead.common.model.messages.QoSReservationResponse;
import eu.arrowhead.common.model.messages.QoSReserve;
import eu.arrowhead.common.model.messages.QoSVerificationResponse;
import eu.arrowhead.common.model.messages.QoSVerifierResponse;
import eu.arrowhead.common.model.messages.QoSVerify;
import eu.arrowhead.core.qos.manager.algorithms.IQoSVerifierAlgorithm;
import eu.arrowhead.core.qos.manager.algorithms.VerifierAlgorithmFactory;
import eu.arrowhead.core.qos.manager.database.model.Network;
import eu.arrowhead.core.qos.manager.database.model.Network_Device;
import eu.arrowhead.core.qos.manager.database.model.QoS_Resource_Reservation;
import eu.arrowhead.core.qos.manager.drivers.DriversFactory;
import eu.arrowhead.core.qos.manager.drivers.QoSDriver;
import eu.arrowhead.core.qos.manager.factories.QoSFactory;
import eu.arrowhead.core.qos.manager.factories.SCSFactory;

public class QoSManagerService {

	private static Logger log = Logger.getLogger(QoSManagerService.class.getName());
	private VerifierAlgorithmFactory algorithmFactory;

	private SCSFactory scsfactory;
	private QoSFactory qosfactory;

	private Client client;

	public QoSManagerService() {
		algorithmFactory = VerifierAlgorithmFactory.getInstance();
		qosfactory = new QoSFactory();
		scsfactory = new SCSFactory();
		client = ClientBuilder.newClient();
	}

	/**
	 * Verifies if the requestedQoS is possible on the selected providers.
	 * 
	 * @param message
	 *            QoSVerify parameters.
	 * @return Returns if is possible or not and why.
	 */
	public QoSVerificationResponse qoSVerify(QoSVerify message) {
		QoSVerificationResponse qosVerificationResponse = new QoSVerificationResponse();
		log.info("QoS: Verifying QoS paramteres.");

		// Get The Consumer Device to get all of its capabilites
		Network_Device consumer_network_device = scsfactory.getNetworkDeviceFromSystem(message.getConsumer());
		// Get The Consumer QoSReservations
		List<QoS_Resource_Reservation> consumerDeviceQoSReservations = qosfactory
				.getQoSReservationsFromArrowheadSystem(message.getConsumer());

		for (ArrowheadSystem system : message.getProvider()) {
			// Get Provider Network - TO GET CAPABILITIES
			Network_Device provider_network_device = scsfactory.getNetworkDeviceFromSystem(system);
			if (provider_network_device == null)
				continue;

			// Get Provider Arrowhead System - TO GET QOSRESERVATIONS
			List<QoS_Resource_Reservation> providerDeviceQoSReservations = qosfactory
					.getQoSReservationsFromArrowheadSystem(system);

			// GET NETWORK TO GET ITS TYPE: ex:FTTSE
			// Network network =
			// scsfactory.getNetworkFromNetworkDevice(provider_network_device);
			Network network = provider_network_device.getNetwork();
			if (network == null)
				continue;

			// Run Algorithm
			IQoSVerifierAlgorithm algorithm = algorithmFactory.getAlgorithm(network.getNetworkType());
			QoSVerifierResponse response = algorithm.verifyQoS(provider_network_device.getNetworkCapabilities(),
					consumer_network_device.getNetworkCapabilities(), providerDeviceQoSReservations,
					consumerDeviceQoSReservations, message.getRequestedQoS(), message.getCommands());

			updateQoSVerificationResponse(system, response, qosVerificationResponse);
		}

		log.info("QoS: QoS paramteres verified.");
		return qosVerificationResponse;
	}

	/**
	 * Reserves a QoS on the consumer and provider stream.
	 * 
	 * @param message
	 *            QoSReservation parameters.
	 * @return Returns if the reservation was possible.
	 * @throws ReservationException
	 *             The reservation on the devices was not possible.
	 * @throws DriverNotFoundException
	 *             The network type doesnt have a driver assigned.
	 */
	public QoSReservationResponse qoSReserve(QoSReserve message) throws ReservationException, DriverNotFoundException {
		QoSReservationResponse qosreservationResponse;

		// Go To System Configuration Store get NetworkDevice
		ArrowheadSystem consumer = message.getConsumer();
		ArrowheadSystem provider = message.getProvider();

		// Get The Consumer Device to get all of its capabilites
		Network_Device consumer_network_device = scsfactory.getNetworkDeviceFromSystem(consumer);
		// Get The Producer Device to get all of its capabilites
		Network_Device provider_network_device = scsfactory.getNetworkDeviceFromSystem(provider);
		if (provider_network_device == null)
			return new QoSReservationResponse(false);

		// TODO: GET NETWORK
		Network network = provider_network_device.getNetwork();
		if (network == null)
			return new QoSReservationResponse(false);
		/*********************************************************
		 ******************* Generate Commands *******************
		 *********************************************************/

		/* FOR BOTH */
		Map<String, String> responseS = DriversFactory.getInstance().generateCommands(network.getNetworkType(),
				network.getNetworkConfigurations(), provider, consumer, message.getService(), message.getCommands(),
				message.getRequestedQoS());

		scsfactory.updateNetwork(network);

		/**
		 * **** IF SUCCESS: Saving Quality of Service Reservation on DataBase *
		 */
		/* Create Message Stream */
		Boolean wasSucessful = qosfactory.saveMessageStream(provider, consumer, message.getService(),
				message.getRequestedQoS(), responseS, network.getNetworkType());

		if (wasSucessful) {
			/** IF SUCCESS: Contact Monitoring Core System **/
			// Send to Monitor
			QoSMonitorAddRule rule = new QoSMonitorAddRule();
			rule.setProvider(provider);
			rule.setConsumer(consumer);
			Boolean softRealTIme = (message.getRequestedQoS().containsKey(QoSDriver.HARD_REAL_TIME)) ? true : false;
			rule.setSoftRealTime(softRealTIme);
			rule.setType(network.getNetworkType().toUpperCase());
			Map<String, String> temp = new HashMap<>();
			temp.putAll(message.getRequestedQoS());
			temp.putAll(network.getNetworkConfigurations());

			rule.setParameters(temp);

			boolean response = contactMonitoring(rule);
			qosreservationResponse = new QoSReservationResponse(response,
					new QoSReservationCommand(message.getService(), message.getProvider(), message.getConsumer(),
							responseS, message.getRequestedQoS()));
			return qosreservationResponse;
		} else {
			// voltar a tras
		}

		return null;
	}

	/**
	 * Fills the parameters of a qosVerify message.
	 * 
	 * @param system
	 *            ArrowheadSystem
	 * @param v
	 *            Was possible.
	 * @param qosVerificationResponse
	 *            Reject Motivation Reason.
	 */
	protected void updateQoSVerificationResponse(ArrowheadSystem system, QoSVerifierResponse v,
			QoSVerificationResponse qosVerificationResponse) {
		boolean isPossible = v.getResponse();
		qosVerificationResponse.addResponse(system, v.getResponse());
		if (!isPossible) {
			qosVerificationResponse.addRejectMotivation(system, v.getRejectMotivation());
		}
	}

	/**
	 * Contacts the QoSMonitoring
	 * 
	 * @param rule
	 *            Message to send to the QoSMonitor.
	 * @return Returns true if it was successful.
	 */
	protected boolean contactMonitoring(QoSMonitorAddRule rule) {
		// TODO - ler de um ficheiro de propriedades
		String strtarget = "http://192.168.60.144:8080/qosmonitor/monitor/rule";
		WebTarget target = client.target(strtarget);
		Response response = target.request().header("Content-type", "application/json").post(Entity.json(rule));
		if (response.getStatus() > 199 && response.getStatus() < 300) {
			return true;
		}
		return false;
	}

	/**
	 * Validates the filters, removing the unkown ones.
	 * 
	 * @param filter
	 *            Filter.
	 * @return Returns Returns the qosReservations using that filter.
	 */
	public FilterQoSReservationsResponse filterReservation(FilterQoSReservations filter) {
		Map<String, String> temp = new HashMap<>();
		if (filter != null && filter.getFilters() != null) {
			temp.putAll(filter.getFilters());
			for (Map.Entry<String, String> entry : temp.entrySet()) {
				if (entry.getKey().compareToIgnoreCase(QoSDriver.BANDWIDTH) == 0 && entry.getValue() != null)
					break;
				if (entry.getKey().compareToIgnoreCase(QoSDriver.DELAY) == 0 && entry.getValue() != null)
					break;
				if (entry.getKey().compareToIgnoreCase(QoSDriver.HARD_REAL_TIME) == 0 && entry.getValue() != null)
					break;
				filter.getFilters().remove(entry.getKey());
			}
		}
		return new FilterQoSReservationsResponse(qosfactory.getQoSReservationFromFilter(temp));
	}

	/**
	 * Deletes a stream configuration between provider and consumer.
	 * @param qosReservation QoSReservation to be deleted.
	 * @return True if was successfull.
	 */
	public boolean deleteQoSReserve(QoSReserve qosReservation) {
		throw new NotYetImplementedException();
	}

}
