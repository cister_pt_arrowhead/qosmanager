package eu.arrowhead.listener;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.log4j.jdbc.JDBCAppender;
import org.hibernate.cfg.Configuration;

import eu.arrowhead.core.qos.manager.database.QoSRepositoryImpl;
import eu.arrowhead.core.qos.manager.database.SCSRepositoryImpl;
import eu.arrowhead.core.qos.manager.register.ServiceRegister;

public class ServletContextClass implements ServletContextListener {

	private static Logger log = Logger.getLogger(ServletContextClass.class.
		getName());
	private static final String MANAGER_REGISTRY_PACKAGE = "eu.arrowhead.core.qos.manager.register.";
	private static final List<String> REGISTERED = new ArrayList<>();

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		System.out.println("[Arrowhead QoSManager] Servlet deployed.");

		//[PT] Connect To QoS Store
		if (connectToStore(SCSRepositoryImpl.URL) && connectToStore(QoSRepositoryImpl.URL)) {
			System.out.println("Conectado a QOS e SCS Stores!");
		} else {
			System.out.println("Não Conectado!");
		}

		//Register QoSManager service in service registry
		List<String> registries = getServiceRegistry();

		for (String registry : registries) {
			try {
				log.
					log(Level.INFO, "Getting " + MANAGER_REGISTRY_PACKAGE + "{0} class.", registry);
				ServiceRegister register = getRegistryClass(registry);
				log.
					log(Level.INFO, "Registering in {0} ServiceRegistry", register.
						getClass().getName());
				if (register.registerQoSManagerService()) {
					REGISTERED.add(registry);
					log.log(Level.INFO, "Registry in {0} successful!", register.
							getClass().getName());
				} else {
					log.
						log(Level.WARNING, "Registry in {0} unsuccessful!", register.
							getClass().getName());
				}
			} catch (ClassNotFoundException ex) {
				String excMessage = "Not registered in registry " + registry + ". "
					+ "Registry class " + registry + " not found. Make "
					+ "sure you have the right registry class for your "
					+ "situation and that it's available in this version "
					+ "and/or not misspelled.";
				log.log(Level.SEVERE, excMessage);
//                throw new RuntimeException(excMessage);
			} catch (InstantiationException | IllegalAccessException ex) {
				log.log(Level.SEVERE, ex.getMessage());
			}
		}

		log.info("[Arrowhead Core] Servlet redeployed.");

	}

	private boolean connectToStore(String store_name) {
		try {
			// Access Hibernate config data
			Configuration configuration = new Configuration().
				configure(store_name);

			// Configure Log4j appender
			JDBCAppender appender = new JDBCAppender();
			appender.setURL(configuration.
				getProperty("hibernate.connection.url"));
			appender.setUser(configuration.
				getProperty("hibernate.connection.username"));
			appender.setPassword(configuration.
				getProperty("hibernate.connection.password"));
			appender.setDriver(configuration.
				getProperty("hibernate.connection.driver_class"));
			return true;
		} catch (Exception e) {
			return false;
		}

	}

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		System.out.println("[Arrowhead Core] Servlet destroyed.");

		/*//Unregister from ServiceRegistry
		for (String registry : REGISTERED) {
			try {
				ServiceRegister register = getRegistryClass(registry);
				register.unregisterQoSMonitorService();
			} catch (ClassNotFoundException ex) {
				String excMessage = "Registry class " + registry + " not found. Make "
					+ "sure you have the right registry class for your "
					+ "situation and that it's available in this version "
					+ "and/or not misspelled.";
				log.log(Level.SEVERE, excMessage);
				throw new RuntimeException(excMessage);
			} catch (InstantiationException | IllegalAccessException ex) {
				log.log(Level.SEVERE, ex.getMessage());
			}
		}*/
		// This manually deregisters JDBC driver, which prevents Tomcat 7 from
		// complaining about memory leaks wrto this class
		Enumeration<Driver> drivers = DriverManager.getDrivers();
		while (drivers.hasMoreElements()) {
			Driver driver = drivers.nextElement();
			try {
				DriverManager.deregisterDriver(driver);
			} catch (SQLException e) {

			}

		}
	}

	/**
	 * Gets a list of ServiceRegistry to register the QoSMonitor service.
	 *
	 * @return list of ServiceRegistry
	 */
	private List<String> getServiceRegistry() {
		Properties props = null;
		String[] registries;
		try {
			props = new Properties();
			InputStream inputStream = getClass().getClassLoader().
				getResourceAsStream("serviceregistry.properties");
			if (inputStream != null) {
				props.load(inputStream);
				inputStream.close();
			} else {
				String exMsg = "Properties file 'serviceregistry.properties' not found in the classpath";
				log.log(Level.SEVERE, exMsg);
				throw new FileNotFoundException(exMsg);
			}
		} catch (Exception ex) {
			log.log(Level.SEVERE, ex.getMessage());
			throw new RuntimeException(ex.getMessage());
		}
		registries = props.getProperty("registry.option").split(",");
		if (registries.length == 0) {
			String exMsg = "No ServiceRegistry values found in registry.option of serviceregistry.properties file.";
			log.log(Level.SEVERE, exMsg);
			throw new RuntimeException(exMsg);
		}
		return Arrays.asList(registries);
	}

	/**
	 * Returns an initialized instance of class, finding it by it's name.
	 *
	 * @param name name of the class to instantiate
	 * @return a Monitor implementation
	 * @throws ClassNotFoundException thrown when trying to initialize a class
	 * that cannot be found. This may be due to the type in the message being
	 * wrong or mistyped
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	public ServiceRegister getRegistryClass(String name)
		throws ClassNotFoundException, InstantiationException, IllegalAccessException {
		Class<?> cl;

		cl = Class.forName(MANAGER_REGISTRY_PACKAGE + name);

		ServiceRegister monitor = (ServiceRegister) cl.newInstance();

		return monitor;
	}

}
