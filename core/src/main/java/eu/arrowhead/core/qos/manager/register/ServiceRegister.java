/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.arrowhead.core.qos.manager.register;

/**
 *
 * @author ID0084D
 */
public interface ServiceRegister {
	/**
	 * Register a service.
	 * 
	 * @return Returns true if successful.
	 */
	public boolean registerQoSManagerService();

	/**
	 * Deletes the service.
	 * 
	 * @return Returns true if successful.
	 */
	public boolean unregisterQoSMonitorService();

}
