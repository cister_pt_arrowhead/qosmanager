package eu.arrowhead.core.qos.manager.drivers;

import java.util.Map;

import eu.arrowhead.common.model.ArrowheadService;
import eu.arrowhead.common.model.ArrowheadSystem;

public interface QoSDriver {

	public final String BANDWIDTH = "bandwidth";
	public final String DELAY = "delay";
	public final String HARD_REAL_TIME = "realTime";

	/**
	 * Configures a stream between a provider and a consumer.
	 * @param networkConfiguration Network configuration on a map.
	 * @param provider ArrowheadSystem.
	 * @param consumer ArrowheadSystem.
	 * @param service ArrowheadService.
	 * @param commands Commands to configure the stream. It can be null.
	 * @param requestedQoS Map of a requestedQoS.
	 * @return Returns the stream configuration parameters.
	 */
	public Map<String, String> configure(Map<String, String> networkConfiguration, ArrowheadSystem provider,
			ArrowheadSystem consumer, ArrowheadService service, Map<String, String> commands,
			Map<String, String> requestedQoS);

	/**
	 * Deletes a stream between a provider and a consumer.
	 * @param networkConfiguration Network configuration on a map.
	 * @param provider ArrowheadSystem.
	 * @param consumer ArrowheadSystem.
	 * @param service ArrowheadService.
	 * @param commands Commands to configure the stream. It can be null.
	 * @param requestedQoS Map of a requestedQoS.
	 * @return Returns true when the deletion was succussfully made.
	 */
	public Boolean delete(Map<String, String> networkConfiguration, ArrowheadSystem provider, ArrowheadSystem consumer,
			ArrowheadService service, Map<String, String> commands, Map<String, String> requestedQoS);

}
