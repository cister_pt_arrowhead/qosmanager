package eu.arrowhead.core.qos.manager.drivers;

public interface CommunicationProtocols {

    public final String FTTSE = "fttse";
    public final String ZIGBEE = "zigbee";

}
