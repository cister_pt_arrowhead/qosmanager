package eu.arrowhead.core.qos.manager.drivers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import eu.arrowhead.common.exception.DriverNotFoundException;
import eu.arrowhead.common.exception.ReservationException;
import eu.arrowhead.common.model.ArrowheadService;
import eu.arrowhead.common.model.ArrowheadSystem;

public class DriversFactory {

	private static DriversFactory instance;
	private static HashMap<String, QoSDriver> driversClasses;

	protected DriversFactory() {
		//
	}

	/**
	 * Returns a instance from this singleton class.
	 * 
	 * @return
	 */
	public static DriversFactory getInstance() {
		if (instance == null) {
			instance = new DriversFactory();
		}
		bootstrap();
		return instance;
	}

	/**
	 * This method is where the drivers are added.
	 **/
	private static void bootstrap() {
		if (driversClasses == null) {
			driversClasses = new HashMap<>();
		}
		driversClasses.put(CommunicationProtocols.FTTSE, (new FTTSE()));
	}

	/**
	 * 
	 * @param networkType Network type (ex. fttse)
	 * @param networkConfiguration Network configuration parameters on a map.
	 * @param provider ArrowheadSystem.
	 * @param consumer ArrowheadSystem.
	 * @param service ArrowheadService.
	 * @param commands Map of the selected commands from the user.
	 * @param requestedQoS Map of the desired requestedQoS.
	 * @return Returns the generatedCommands from the QoSDriver.
	 * @throws ReservationException The StreamConfiguration found an error.
	 * @throws DriverNotFoundException The selected type doesnt have an assigned driver.
	 */
	public Map<String, String> generateCommands(String networkType, Map<String, String> networkConfiguration,
			ArrowheadSystem provider, ArrowheadSystem consumer, ArrowheadService service, Map<String, String> commands,
			Map<String, String> requestedQoS) throws ReservationException, DriverNotFoundException {

		Map<String, String> streamConfiguration = driversClasses.get(networkType).configure(networkConfiguration,
				provider, consumer, service, commands, requestedQoS);
		if (streamConfiguration == null) {
			throw new ReservationException();
		}

		return streamConfiguration;

	}

}
