package eu.arrowhead.core.qos.manager.algorithms;

import java.util.HashMap;
import java.util.Map;

import eu.arrowhead.core.qos.manager.drivers.CommunicationProtocols;

public class VerifierAlgorithmFactory {

    private static VerifierAlgorithmFactory instance;
    private static Map<String, IQoSVerifierAlgorithm> list;

    private VerifierAlgorithmFactory() {

    }

    public static VerifierAlgorithmFactory getInstance() {
        if (instance == null) {
            instance = new VerifierAlgorithmFactory();
        }
        bootstrap();
        return instance;
    }

    /**
     * get algorithm from a type
     * @param type String type
     * @return returns the algorithm
     */
    public IQoSVerifierAlgorithm getAlgorithm(String type) {
        if (type.equalsIgnoreCase(CommunicationProtocols.FTTSE)) {
            return new FTTSE_Algorithm();
        }

        // manda exceção
        return null;
    }

    /**
     * adds the algorithms
     */
    private static void bootstrap() {
        if (list == null) {
            list = new HashMap<>();
        }

        list.put(CommunicationProtocols.FTTSE, new FTTSE_Algorithm());

    }

}
