package eu.arrowhead.core.qos.manager.algorithms;

import java.util.List;
import java.util.Map;

import eu.arrowhead.common.model.messages.QoSVerifierResponse;
import eu.arrowhead.core.qos.manager.database.model.QoS_Resource_Reservation;

public interface IQoSVerifierAlgorithm {
	/**
	 * Verify if the desired qos is possible
	 * 
	 * @param provierDeviceCapabilities
	 *            map with capabilities of the provider system
	 * @param consumerDeviceCapabilities
	 *            map with capabilities of the consumer system
	 * @param providerDeviceQoSReservations
	 *            list with reservations of the provider system
	 * @param consumerDeviceQoSReservations
	 *            list with reservation of the consumer system
	 * @param requestedQoS
	 *            map woth the requested qos
	 * @param commands
	 *            stream configuration commands
	 * @return return true or false with a rejetction motivation
	 */
	public QoSVerifierResponse verifyQoS(Map<String, String> provierDeviceCapabilities,
			Map<String, String> consumerDeviceCapabilities,
			List<QoS_Resource_Reservation> providerDeviceQoSReservations,
			List<QoS_Resource_Reservation> consumerDeviceQoSReservations, Map<String, String> requestedQoS,
			Map<String, String> commands);

}
