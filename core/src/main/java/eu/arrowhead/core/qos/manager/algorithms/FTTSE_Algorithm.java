package eu.arrowhead.core.qos.manager.algorithms;

import java.util.List;
import java.util.Map;

import eu.arrowhead.common.model.messages.QoSVerifierResponse;
import eu.arrowhead.core.qos.manager.database.model.QoS_Resource_Reservation;

public class FTTSE_Algorithm implements IQoSVerifierAlgorithm {

	public FTTSE_Algorithm() {

	}

	@Override
	public QoSVerifierResponse verifyQoS(
		Map<String, String> provierDeviceCapabilities,
		Map<String, String> consumerDeviceCapabilities,
		List<QoS_Resource_Reservation> providerDeviceQoSReservations,
		List<QoS_Resource_Reservation> consumerDeviceQoSReservations,
		Map<String, String> requestedQoS, Map<String, String> commands) {

		QoSVerifierResponse response = new QoSVerifierResponse();
		response.setResponse(true);

		return response;
	}

}
