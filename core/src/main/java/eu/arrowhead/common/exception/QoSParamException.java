package eu.arrowhead.common.exception;

public class QoSParamException extends Exception {

    public QoSParamException() {
        super();
    }

    public QoSParamException(String message) {
        super(message);
    }

}
